import Vue from "vue/dist/vue.esm.js"
import {VueGoodTable} from 'vue-good-table';

let tagArticlesApp = new Vue({
	el:"#tagArticlesApp",
	components: {
	  VueGoodTable,
	},
	data:{
		selected:{
			region:null
		},
		options:{
			region:[]
		},
		tables:{
			digital:{
				columns:[
					{
						label:"Article",
						field:"article"
					},
					{
						label:"Link",
						field:"link"
					},
					{
						label:"Tags",
						field:"tags"
					}
				],
				rows:[]
			},
			print:{
				columns:[
					{
						label:"Article",
						field:"article"
					},
					{
						label:"Publication",
						field:"publication"
					},
					{
						label:"Tags",
						field:"tags"
					}
				],
				rows:[]
			}
		}
	},
	created:function(){
		
		this.options.region = Object.keys(db_dynamic)

		this.selected.region = this.options.region.includes('Uncategorised') ? "Uncategorised" : this.options.region[0]

	},
	watch:{
		"selected.region":function(){
			this.populateTable()
		}
	},
	methods: {
		populateTable(){
			let digital_tableData = []
			let print_tableData = []

			for(const row of db_dynamic[this.selected.region].digital){
				digital_tableData.push(row)
			}

			for(const row of db_dynamic[this.selected.region].print){
				print_tableData.push(row)
			}

			this.tables.print.rows = print_tableData
			this.tables.digital.rows = digital_tableData
		}
	}
})

