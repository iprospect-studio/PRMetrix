import Vue from "vue/dist/vue.esm.js"
import Chart from 'chart.js/auto';
import chroma from 'chroma-js';


let consumerReviewApp = new Vue({
	el:'#consumer_review',
	data:{
		month: null,
		options:{
			brand:['All','BH','DWH'],
			region:['All'],
			division:['All'],
		},
		selected:{
			brand:[],
			region:[],
			division:[]
		},
		chart_data:{
			sov:{
				labels:[],
				data:[]
			},
			sentiment:{
				labels:[],
				data:[]
			}
		},
		master:{
			dynamic:db_dynamic,
			static:db_static,
		},
		scores:{
			online:{
				performance:{
					organic:{
						score:0,
						change:0
					},
					referral:{
						score:0,
						change:0
					},
					enquiries:{
						score:0,
						change:0
					},
					links:{
						score:0,
						change:0
					},
				}
			},
			digital:{
				all:{
					coverage:{
						score:0,
						change:0
					},
					links:{
						score:0,
						change:0
					},
					social_shares:{
						score:0,
						change:0
					},
				},
				crisis:{
					coverage:{
						score:0,
						change:0
					},
					links:{
						score:0,
						change:0
					},
					social_shares:{
						score:0,
						change:0
					},
				},
				non_crisis:{
					coverage:{
						score:0,
						change:0
					},
					links:{
						score:0,
						change:0
					},
					social_shares:{
						score:0,
						change:0
					},
				},
				campaigns:{
					coverage:{
						score:0,
						change:0
					},
					links:{
						score:0,
						change:0
					},
					social_shares:{
						score:0,
						change:0
					},
				}
			},
			print:{
				all:{
					coverage:{
						score:0,
						change:0
					},
				},
				crisis:{
					coverage:{
						score:0,
						change:0
					},
				},
				non_crisis:{
					coverage:{
						score:0,
						change:0
					},
				},
			}
		}
	},
	created:function(){
		const monthNames = ["January", "February", "March", "April", "May", "June",
		  "July", "August", "September", "October", "November", "December"
		];

		const d = new Date();
		this.month = monthNames[d.getMonth()];

		this.selected.brand = this.options.brand[0]
		this.selected.region = this.options.region[0]
		this.selected.division = this.options.division[0]

		this.getStaticData()
		this.getDynamicData()
	},
	watch:{
		'selected.region': function(){
			if (this.selected.region == 'All'){
				this.options.division = ['All']
				this.selected.division = this.options.division[0]
				// get brand data
				console.log('gret brand data', this.selected.brand)

			}else{
				// get divisions
				let divisions = Object.keys(db_dynamic.pr_coverage[this.selected.brand][this.selected.region])
				// this.options.division = ['All']
				this.options.division = divisions
				this.selected.division = this.options.division[0]

				// get Regions data
				console.log('gret regions data', this.selected.region)
				this.getDynamicData()

			}
		},
		'selected.division': function(){
			if (this.selected.division == 'All'){		
				// get Regions data
				console.log('gret regions data', this.selected.region)
				
			}else{
				// get divisions data
				console.log('gret divisions data', this.selected.division)
			}

			this.getDynamicData()

		},
		'selected.brand': function(){
			let regions = Object.keys(db_dynamic.pr_coverage[this.selected.brand])
			// this.options.division = ['All']
			this.options.region = regions

			this.getDynamicData()
		}
	},
	mounted:function(){

		let colors = chroma.scale(['#800080', '#ff0000', '#ffff00', '#008000', '#0000ff']).colors(this.chart_data.sov.labels.length)
        
		const chartShareOfVoice = new Chart('shareOfVoice', {
		    type: 'doughnut',
		    data: {
		        labels: this.chart_data.sov.labels,
		        datasets: [{
		            label: '# of Votes',
		            data: this.chart_data.sov.data,
		            backgroundColor: colors,
		            borderColor: colors,
		            borderWidth: 1
		        }]
		    },
		    options: {
		    	maintainAspectRatio:false,
		    	responsive:true
		    }
		});

		const chartAverageSentiment = new Chart('averageSentiment', {
		    type: 'bar',
		    data: {
		        labels: this.chart_data.sentiment.labels,
		        datasets: [{
		            label: '# of Votes',
		            data: this.chart_data.sentiment.data,
		            backgroundColor: colors,
		            borderColor: colors,
		            borderWidth: 1
		        }]
		    },
		    options: {
		    	maintainAspectRatio:false,
		    	responsive:true,
		        scales: {
		            y: {
		                beginAtZero: true
		            }
		        }
		    }
		});
	},
	methods:{
		getDynamicData:function(){

			// console.warn('this.selected.brand',this.selected.brand)
			// console.warn('this.selected.region',this.selected.region)
			// console.warn('this.selected.division',this.selected.division)

			let data = {}
			let brand = this.selected.brand
			console.warn("selected:", this.selected.brand +":"+this.selected.region+":"+this.selected.division)
			data["pr_coverage"] = this.master.dynamic["pr_coverage"][this.selected.brand][this.selected.region][this.selected.division]
			data["new_homes"] = this.master.dynamic["new_homes"][this.selected.brand][this.selected.region][this.selected.division]
			data["digital_coverage"] = this.master.dynamic["digital_coverage"][this.selected.brand][this.selected.region][this.selected.division]

			console.warn("data", data)
			this.scores.online.performance.organic.score = data.new_homes.organic.score
			this.scores.online.performance.organic.change = data.new_homes.organic.change
			this.scores.online.performance.referral.score = data.new_homes.referral.score
			this.scores.online.performance.referral.change = data.new_homes.referral.change
			this.scores.online.performance.enquiries.score = data.new_homes.enquiries.score
			this.scores.online.performance.enquiries.change = data.new_homes.enquiries.change
			this.scores.online.performance.links.score = data.new_homes.links.score
			this.scores.online.performance.links.change = data.new_homes.links.change

			this.scores.digital.all.coverage.score = data.digital_coverage.all.coverage.score
			this.scores.digital.all.coverage.change = data.digital_coverage.all.coverage.change
			this.scores.digital.all.links.score = data.digital_coverage.all.links.score
			this.scores.digital.all.links.change = data.digital_coverage.all.links.change
			this.scores.digital.all.social_shares.score = data.digital_coverage.all.shares.score
			this.scores.digital.all.social_shares.change = data.digital_coverage.all.shares.change
			this.scores.digital.crisis.coverage.score = data.digital_coverage.crisis.coverage.score
			this.scores.digital.crisis.coverage.change = data.digital_coverage.crisis.coverage.change
			this.scores.digital.non_crisis.coverage.score = data.digital_coverage.non_crisis.coverage.score
			this.scores.digital.non_crisis.coverage.change = data.digital_coverage.non_crisis.coverage.change

			this.scores.print.all.coverage.score = data.pr_coverage.all.coverage.score
			this.scores.print.all.coverage.change = data.pr_coverage.all.coverage.change
			this.scores.print.crisis.coverage.score = data.pr_coverage.crisis.coverage.score
			this.scores.print.crisis.coverage.change = data.pr_coverage.crisis.coverage.change
			this.scores.print.non_crisis.coverage.score = data.pr_coverage.non_crisis.coverage.score
			this.scores.print.non_crisis.coverage.change = data.pr_coverage.non_crisis.coverage.change

		},
		getStaticData:function(){
			// campaigns data
			this.scores.digital.campaigns.coverage.score = this.master.static['Campaigns']['Coverage'].score
			this.scores.digital.campaigns.coverage.change = this.master.static['Campaigns']['Coverage'].change
			this.scores.digital.campaigns.links.score = this.master.static['Campaigns']['Links'].score
			this.scores.digital.campaigns.links.change = this.master.static['Campaigns']['Links'].change
			this.scores.digital.campaigns.social_shares.score = this.master.static['Campaigns']['Social Shares'].score
			this.scores.digital.campaigns.social_shares.change = this.master.static['Campaigns']['Social Shares'].change

			// chart data
			const sov = db_static["SOV"]
			for (const key in sov){
				this.chart_data.sov.labels.push(key)
				this.chart_data.sov.data.push(sov[key])
			}

			const sentiment = db_static["Sentiment"]
			for (const key in sentiment){
				this.chart_data.sentiment.labels.push(key)
				this.chart_data.sentiment.data.push(sentiment[key])
			}

		},
	}

})