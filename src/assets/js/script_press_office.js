import Vue from "vue/dist/vue.esm.js"
import Chart from 'chart.js/auto';
import {VueGoodTable} from 'vue-good-table';

let pressOfficeApp = new Vue({
	el:"#pressOffice",
	components: {
	  VueGoodTable,
	},
	data:{
		monthNames: ["January", "February", "March", "April", "May", "June",
		  "July", "August", "September", "October", "November", "December"
		],
		dates:{},
		selected:{
			brand:'All',
			year:null,
			month:null
		},
		options:{
			brand:['All', 'BH', 'DWH'],
			year:[],
			month:[]
		},
		tables:{
			digital_region:{
				columns:[
					{
						label:'Region',
						field:'region'
					},{
						label:'Coverage',
						field:'coverage'
					},{
						label:'MoM',
						field:'mom'
					},{
						label:'Links',
						field:'links'
					},{
						label:'Follow',
						field:'follow'
					}
				],
				rows:[]
			},
			digital_division:{
				columns:[
					{
						label:'Division',
						field:'division'
					},{
						label:'Coverage',
						field:'coverage'
					},{
						label:'Links',
						field:'links'
					},{
						label:'Follow',
						field:'follow'
					}
				],
				rows:[]
			},
			print_region:{
				columns:[
					{
						label:'Region',
						field:'region'
					},{
						label:'Coverage',
						field:'coverage'
					},{
						label:'MoM',
						field:'mom'
					}
				],
				rows:[]
			},
			print_division:{
				columns:[
					{
						label:'Division',
						field:'division'
					},{
						label:'Coverage',
						field:'coverage'
					}
				],
				rows:[]
			},
		},
	},
	created:function(){
		// get all years and months
		for(const row of db_dynamic){

			let dateObj = new Date(row.date)
			let year = dateObj.getFullYear()
			let month = dateObj.getMonth()

			if (!this.dates[year]){
				this.dates[year] = []
			}
			this.dates[year].push(month)
		}


		// remove dups & sort dates into month order
		let years = Object.keys(this.dates)
		years.sort()
		for(let year of years){
			this.dates[year] = [...new Set(this.dates[year])]
			this.dates[year].sort((a,b)=>{
				return a-b
			})
		}
		
		// set options
		this.options.year = years
		this.selected.year = this.options.year[this.options.year.length-1]

	},
	watch:{
		"selected.year":function(){
			let months = this.dates[this.selected.year]
			this.options.month = months
			if (this.selected.month == null){
				this.selected.month = this.options.month[this.options.month.length-1]
			}else{
				if (!this.options.month.includes(this.selected.month)){
					this.selected.month = this.options.month[0]
				}
			}
		},
		"selected.month":function(){
			this.populateTable()
		},
		"selected.brand":function(){
			this.populateTable()
		}
	},
	methods: {
		populateTable(){
			let tableData = {
				digital_region:[],
				digital_division:[],
				print_region:[],
				print_division:[]
			}

			const month = this.selected.month
			const year = this.selected.year

			for(const row of db_dynamic){
				let date = new Date(row.date)

				if(date.getFullYear() == year && date.getMonth() == month){
					tableData.digital_region = row.data[this.selected.brand].digital_region
					tableData.digital_division = row.data[this.selected.brand].digital_division
					tableData.print_region = row.data[this.selected.brand].print_region
					tableData.print_division = row.data[this.selected.brand].print_division
				}
			}

			this.tables.digital_region.rows = tableData.digital_region
			this.tables.digital_division.rows = tableData.digital_division
			this.tables.print_region.rows = tableData.print_region
			this.tables.print_division.rows = tableData.print_division
		}
	}
})

