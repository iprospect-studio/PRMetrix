import Vue from "vue/dist/vue.esm.js"
import Chart from 'chart.js/auto';
import {VueGoodTable} from 'vue-good-table';


let newHomesApp = new Vue({
	el:'#newHomesApp',
	components: {
	  VueGoodTable,
	},
	data:{
		monthNames: ["January", "February", "March", "April", "May", "June",
		  "July", "August", "September", "October", "November", "December"
		],
		dates:{},
		options:{
			year:[],
			month:[],
			brand:['All','BH','DWH'],
			region:['All'],
			division:['All'],
		},
		selected:{
			year:null,
			month:null,
			brand:[],
			region:[],
			division:[]
		},
		chart_data:{
			labels:[],
			data:[]
		},
		scores:{
			organic:{
			  score:1,
			  change:0
			},
			referral:{
			  score:1,
			  change:0
			},
			enquiries:{
			  score:1,
			  change:0
			},
			links:{
			  score:1,
			  change:0
			},
		},
		chart:null,
		table:{
			columns:[
				{
					label:'Area',
					field:'area'
				},{
					label:'URL',
					field:'url'
				},{
					label:'Organic',
					field:'coverage'
				},{
					label:'Enquiries',
					field:'leads'
				},{
					label:'Links',
					field:'links'
				},{
					label:'Follow Links',
					field:'follow_links'
				},
			],
			rows:[]
		}
	},
	created:function(){
		const monthNames = ["January", "February", "March", "April", "May", "June",
		  "July", "August", "September", "October", "November", "December"
		];

		// get all years and months
		for(const row of db_dynamic.monthly){
			let dateObj = new Date(row.date)
			let year = dateObj.getFullYear()
			let month = dateObj.getMonth()

			if (!this.dates[year]){
				this.dates[year] = []
			}
			this.dates[year].push(month)
		}

		// remove dups & sort dates into month order
		let years = Object.keys(this.dates)
		years.sort()
		for(let year of years){
			this.dates[year] = [...new Set(this.dates[year])]
			this.dates[year].sort((a,b)=>{
				return a-b
			})
		}

		console.log(this.dates)

		
		// set options
		this.options.year = years
		this.selected.year = this.options.year[this.options.year.length-1]

		this.selected.brand = this.options.brand[0]

		let regions = Object.keys(db_dynamic.monthly[0].data[this.selected.brand])
		// this.options.division = ['All']
		this.options.region = regions


		this.selected.region = this.options.region[0]
		this.selected.division = this.options.division[0]

		this.getTableData()
	},
	watch:{
		"selected.year":function(){
			let months = this.dates[this.selected.year]
			this.options.month = months
			if (this.selected.month == null){
				this.selected.month = this.options.month[this.options.month.length-1]
			}else{
				if (!this.options.month.includes(this.selected.month)){
					this.selected.month = this.options.month[0]
				}
			}
		},
		'selected.region': function(){
			if (this.selected.region == 'All'){
				this.options.division = ['All']
				this.selected.division = this.options.division[0]
				// get brand data
				console.log('gret brand data', this.selected.brand)

			}else{
				// get divisions
				let divisions = Object.keys(db_dynamic.monthly[0].data[this.selected.brand][this.selected.region])
				// this.options.division = ['All']
				this.options.division = divisions
				this.selected.division = this.options.division[0]

				// get Regions data
				console.log('gret regions data', this.selected.region)
			}
			this.getStatData()
			this.buildChart()
		},
		'selected.division': function(){
			this.getStatData()
			this.buildChart()
		},
		'selected.brand': function(){
			this.getStatData()
			this.buildChart()
		},
		"selected.month":function(){
			this.getStatData()
		},
	},
	mounted:function(){
		this.chart = new Chart('sessionOverTime', {
		    type: 'line',
		    data: {
		        labels: this.chart_data.labels,
		        datasets: [{
		            label: 'Sesions',
	                data: this.chart_data.data,
	                fill: false,
	                borderColor: 'rgb(31, 199, 255)',
	                backgroundColor: 'rgb(31, 199, 255)',
	                tension: 0.1
		        }]
		    },
		    options: {
		    	maintainAspectRatio:false,
		    	responsive:true
		    }
		});
	},
	methods:{
		getStatData:function(){
			const month = this.selected.month
			const year = this.selected.year

			for(const row of db_dynamic.monthly){
				let date = new Date(row.date)

				if(date.getFullYear() == year && date.getMonth() == month){
					this.scores.organic.score = row.data[this.selected.brand][this.selected.region][this.selected.division].organic.score
					this.scores.organic.change = row.data[this.selected.brand][this.selected.region][this.selected.division].organic.change
					this.scores.referral.score = row.data[this.selected.brand][this.selected.region][this.selected.division].referral.score
					this.scores.referral.change = row.data[this.selected.brand][this.selected.region][this.selected.division].referral.change
					this.scores.enquiries.score = row.data[this.selected.brand][this.selected.region][this.selected.division].enquiries.score
					this.scores.enquiries.change = row.data[this.selected.brand][this.selected.region][this.selected.division].enquiries.change
					this.scores.links.score = row.data[this.selected.brand][this.selected.region][this.selected.division].links.score
					this.scores.links.change = row.data[this.selected.brand][this.selected.region][this.selected.division].links.change

				}
			}
		},
		getChartData:function(){
			
			this.chart_data.labels = []
			this.chart_data.data = []

			const previousMonths = function(num){
			  const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

			  const today = new Date();
			  let d;
			  let arr = []

			  for(var i = num; i > 0; i -= 1) {
			    d = new Date(today.getFullYear(), today.getMonth() - i, 1);
			    arr.push(monthNames[d.getMonth()])
			  }

			  return arr
			}

			this.chart_data.data = db_dynamic.chart[this.selected.brand][this.selected.region][this.selected.division]
			this.chart_data.labels = previousMonths(this.chart_data.data.length)


		},
		getTableData:function(){
			this.table.rows = db_static
		},
		buildChart:function(){
			this.getChartData()
			this.chart.data.labels = this.chart_data.labels
			this.chart.data.datasets[0].data = this.chart_data.data
			this.chart.update()
		}
	}

})