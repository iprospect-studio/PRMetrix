import Vue from "vue/dist/vue.esm.js"
import Chart from 'chart.js/auto';
import {VueGoodTable} from 'vue-good-table';

let campaignsApp = new Vue({
	el:"#campaignsInsightsApp",
	components: {
	  VueGoodTable,
	},
	data:{
		monthNames: ["January", "February", "March", "April", "May", "June",
		  "July", "August", "September", "October", "November", "December"
		],
		dates:{},
		selected:{
			year:null,
			month:null
		},
		options:{
			year:[],
			month:[]
		},
		columns:[
			{
				label:'Url',
				field:'url'
			},{
				label:'Traffic',
				field:'traffic'
			},{
				label:'Leads',
				field:'leads'
			},{
				label:'Coverage',
				field:'coverage'
			},{
				label:'Links',
				field:'links'
			},{
				label:'Follow links',
				field:'follow_links'
			},
		],
		rows:[],
		info:{
      		campaign_name:"campaign name",
	        traffic:{
	        	score:0,
	        	change:0
	        },
	        leads:{
	        	score:0,
	        	change:0
	        },
	        referrals:{
	        	score:0,
	        	change:0
	        },
	        coverage:{
	        	score:0,
	        	change:0
	        }, 
	        enquiries:{
	        	score:0,
	        	change:0
	        },
	        links:{
	        	score:0,
	        	change:0
	        },
	        follow_links:{
	        	score:0,
	        	change:0
	        },
      	}
	},
	created:function(){
		// get all years and months
		for(const row of db_dynamic){
			let dateObj = new Date(row.date)
			let year = dateObj.getFullYear()
			let month = dateObj.getMonth()

			if (!this.dates[year]){
				this.dates[year] = []
			}
			this.dates[year].push(month)
		}

		// remove dups & sort dates into month order
		let years = Object.keys(this.dates)
		years.sort()
		for(let year of years){
			this.dates[year] = [...new Set(this.dates[year])]
			this.dates[year].sort((a,b)=>{
				return a-b
			})
		}
		
		// set options
		this.options.year = years
		this.selected.year = this.options.year[this.options.year.length-1]

	},
	watch:{
		"selected.year":function(){
			let months = this.dates[this.selected.year]
			this.options.month = months
			if (this.selected.month == null){
				this.selected.month = this.options.month[this.options.month.length-1]
			}else{
				if (!this.options.month.includes(this.selected.month)){
					this.selected.month = this.options.month[0]
				}
			}
		},
		"selected.month":function(){
			this.populateTable()
		}
	},
	methods: {
		populateTable(){
			let tableData = []
			let info = {}

			const month = this.selected.month
			const year = this.selected.year

			for(const row of db_dynamic){
				let date = new Date(row.date)

				if(date.getFullYear() == year && date.getMonth() == month){
					tableData = row.urls
					info = row.info
				}
			}

			this.info = info
			this.rows = tableData

			console.log(this.info)
		}
	}
})

