import Vue from "vue/dist/vue.esm.js"
import Chart from 'chart.js/auto';
import {VueGoodTable} from 'vue-good-table';

let pressOfficeApp = new Vue({
	el:"#pressOfficeInsight",
	components: {
	  VueGoodTable,
	},
	data:{
		region:null,
		monthNames: ["January", "February", "March", "April", "May", "June",
		  "July", "August", "September", "October", "November", "December"
		],
		dates:{},
		selected:{
			brand:'All',
			year:null,
			month:null
		},
		options:{
			brand:['All', 'BH', 'DWH'],
			year:[],
			month:[]
		},
		score:{
			traffic:{
				score:0,
				change:0
			}
		},
		table_digital:{
			columns:[
				{
					label:'Brand',
					field:'brand'
				},{
					label:'Site',
					field:'site'
				},{
					label:'Article',
					field:'article'
				},{
					label:'URL',
					field:'url'
				},{
					label:'Has link?',
					field:'has_link'
				},{
					label:'Linking URL',
					field:'linking_url'
				},{
					label:'Social shares',
					field:'shares'
				}
			],
			rows:[
			]
		},
		table_press:{
			columns:[
				{
					label:'Brand',
					field:'brand'
				},{
					label:'Publication',
					field:'publication'
				},{
					label:'Article',
					field:'article'
				}
			],
			rows:[
			]
		},
	},
	created:function(){
		// get region
		const queryString = window.location.search;
		const urlParams = new URLSearchParams(queryString);
		this.region = urlParams.get('region')

		// get all years and months
		for(const row of db_dynamic){
			let dateObj = new Date(row.date)
			let year = dateObj.getFullYear()
			let month = dateObj.getMonth()

			if (!this.dates[year]){
				this.dates[year] = []
			}
			this.dates[year].push(month)
		}

		// remove dups & sort dates into month order
		let years = Object.keys(this.dates)
		years.sort()
		for(let year of years){
			this.dates[year] = [...new Set(this.dates[year])]
			this.dates[year].sort((a,b)=>{
				return a-b
			})
		}
		
		// set options
		this.options.year = years
		this.selected.year = this.options.year[this.options.year.length-1]

	},
	watch:{
		"selected.year":function(){
			let months = this.dates[this.selected.year]
			this.options.month = months
			if (this.selected.month == null){
				this.selected.month = this.options.month[this.options.month.length-1]
			}else{
				if (!this.options.month.includes(this.selected.month)){
					this.selected.month = this.options.month[0]
				}
			}
		},
		"selected.month":function(){
			this.populateTable()
		},
		"selected.brand":function(){
			this.populateTable()
		}
	},
	methods: {
		populateTable(){
			let tableData_digital = []
			let tableData_press = []
			let traffic = []

			const month = this.selected.month
			const year = this.selected.year

			for(const row of db_dynamic){
				let date = new Date(row.date)

				if(date.getFullYear() == year && date.getMonth() == month){
					tableData_digital = row.data.digital_coverage[this.selected.brand].coverage
					tableData_press = row.data.press_coverage[this.selected.brand].coverage
					traffic = row.data.digital_coverage[this.selected.brand].traffic
				}
			}

			// console.warn(this.table.rows)
			
			this.table_digital.rows = tableData_digital
			this.table_press.rows = tableData_press
			this.score.traffic = traffic

			// console.warn(this.table.row)

		}
	}
})

